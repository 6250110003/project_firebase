import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:google_fonts/google_fonts.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      // Remove the debug banner
      debugShowCheckedModeBanner: false,
      title: 'Student',
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // text fields' controllers
  final TextEditingController _fnameController = TextEditingController();
  final TextEditingController _scoreController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _lnameController = TextEditingController();

  // Create a CollectionReference called _products that references the firestore collection
  final CollectionReference _students =
      FirebaseFirestore.instance.collection('student');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      _fnameController.text = documentSnapshot['name'];
      _scoreController.text = documentSnapshot['score'].toString();
      _emailController.text = documentSnapshot['email'];
      _lnameController.text = documentSnapshot['lname'];
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TextField(
                  controller: _fnameController,
                  decoration: const InputDecoration(
                    labelText: 'Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        const Radius.circular(100.0),
                      ),
                    ),
                    prefixIcon: Icon(Icons.person_add),
                  ),
                  style: GoogleFonts.luckiestGuy(fontSize: 18),
                ),
                TextField(
                  controller: _lnameController,
                  decoration: const InputDecoration(
                    labelText: 'Lastname',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        const Radius.circular(100.0),
                      ),
                    ),
                    prefixIcon: Icon(Icons.person),
                  ),
                  style: GoogleFonts.luckiestGuy(fontSize: 18),
                ),
                TextField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        const Radius.circular(100.0),
                      ),
                    ),
                    prefixIcon: Icon(Icons.email_outlined),
                  ),
                  style: GoogleFonts.luckiestGuy(fontSize: 18),
                ),
                TextField(
                  keyboardType:
                      const TextInputType.numberWithOptions(decimal: true),
                  style: GoogleFonts.luckiestGuy(fontSize: 18),
                  controller: _scoreController,
                  decoration: const InputDecoration(
                    labelText: 'Score',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(
                        const Radius.circular(100.0),
                      ),
                    ),
                    prefixIcon: Icon(Icons.score),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String name = _fnameController.text;
                    final String lname = _lnameController.text;
                    final String email = _emailController.text;
                    final double score = double.tryParse(_scoreController.text);
                    if (name != null && score != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _students
                            .add({
                              "name": name,
                              "lname": lname,
                              "email": email,
                              "score": score
                            })
                            .then((value) => print("student Added"))
                            .catchError((error) =>
                                print("Failed to add score: $error"));
                      }

                      if (action == 'update') {
                        // Update the product
                        await _students
                            .doc(documentSnapshot.id)
                            .update({
                              "name": name,
                              "lname": lname,
                              "email": email,
                              "score": score
                            })
                            .then((value) => print("student Updated"))
                            .catchError((error) =>
                                print("Failed to update student: $error"));
                      }

                      // Clear the text fields
                      _fnameController.text = '';
                      _scoreController.text = '';
                      _lnameController.text = '';
                      _emailController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleting a product by id
  Future<void> _deleteProduct(String productId) async {
    await _students
        .doc(productId)
        .delete()
        .then((value) => print("Student Deleted"))
        .catchError((error) => print("Failed to delete student: $error"));

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a student')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Using StreamBuilder to display all products from Firestore in real-time
      body: StreamBuilder(
        stream: _students.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                    streamSnapshot.data.docs[index];
                return Card(
                  color: Colors.lightBlueAccent,
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    title: Text(documentSnapshot['name']),
                    subtitle: Text(documentSnapshot['score'].toString()),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          // Press this button to edit a single product
                          IconButton(
                              icon: const Icon(
                                Icons.edit,
                                color: Colors.black,
                              ),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          // This icon button is used to delete a single product
                          IconButton(
                              icon: const Icon(
                                Icons.delete,
                                color: Colors.red,
                              ),
                              onPressed: () =>
                                  _deleteProduct(documentSnapshot.id)),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      // Add new product
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
    );
  }
}
